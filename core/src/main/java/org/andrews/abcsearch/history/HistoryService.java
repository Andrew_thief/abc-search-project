package org.andrews.abcsearch.history;

import org.andrews.abcsearch.user.ApplicationUser;
import org.andrews.abcsearch.user.ApplicationUserService;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class HistoryService {

    public ApplicationUserService applicationUserService;

    public HistoryService(ApplicationUserService applicationUserService) {
        this.applicationUserService = applicationUserService;
    }

    public void saveRecord(ApplicationUser user, HistoryRecord historyRecord) {
        user.addRecord(historyRecord);

        applicationUserService.save(user);
    }

    public List<HistoryRecord> getAllRecords(ApplicationUser user) {
        return user.getRecords().stream()
                .sorted(Comparator.comparing(HistoryRecord::getCreationDate, Comparator.reverseOrder()))
                .collect(Collectors.toList());
    }

    public void deleteAllRecords(ApplicationUser user) {
        user.setRecords(null);

        applicationUserService.save(user);
    }

}
