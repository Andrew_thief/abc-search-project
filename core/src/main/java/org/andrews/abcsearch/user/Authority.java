package org.andrews.abcsearch.user;

public enum Authority {

    ROLE_USER,
    ROLE_ADMIN,

}
