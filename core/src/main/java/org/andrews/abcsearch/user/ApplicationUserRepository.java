package org.andrews.abcsearch.user;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationUserRepository extends CrudRepository<ApplicationUser, Long> {

    @Query("select * from application_user where email = :email")
    ApplicationUser findByEmail(@Param("email") String email);

    @Query("select exists(select * from application_user where email = :email)")
    Boolean isEmailInUse(@Param("email") String email);
}
