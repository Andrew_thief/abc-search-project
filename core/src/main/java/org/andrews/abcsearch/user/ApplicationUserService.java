package org.andrews.abcsearch.user;

import org.andrews.abcsearch.exceptions.UserNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class ApplicationUserService {

    private final ApplicationUserRepository applicationUserRepo;

    public ApplicationUserService(ApplicationUserRepository applicationUserRepo) {
        this.applicationUserRepo = applicationUserRepo;
    }

    public ApplicationUser save(ApplicationUser user) {
        return applicationUserRepo.save(user);
    }


    public boolean isEmailInUse(String email) {
        return applicationUserRepo.isEmailInUse(email);
    }

    public ApplicationUser getByEmail(String email) {
        ApplicationUser user = applicationUserRepo.findByEmail(email);
        if (user == null) {
            throw new UserNotFoundException("User with email [" + email + "] not found!");
        } else {
            return user;
        }
    }

}


