package org.andrews.abcsearch.search;

import org.andrews.abcsearch.pageparser.*;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ForkJoinPool;


@Service
public class SearchService {


    private final PageParserService pageParserService;

    private final PageDao pageDao;

    private final int MAX_DEPTH;

    private static final int START_DEPTH = 0;

    private final Logger logger;

    public SearchService(PageParserService pageParserService,
                         PageDao pageDao,
                         @Value("${max-depth}") int maxDepth,
                         Logger logger) {
        this.pageParserService = pageParserService;
        this.MAX_DEPTH = maxDepth;
        this.pageDao = pageDao;
        this.logger = logger;
    }

    public List<Page> search(String query, int page) {
        List<Page> pages = new ArrayList<>();
        try {
            List<PageInfo> pageInfos = pageDao.search(query, page);
            pages.addAll(PageMapper.mapToPage(pageInfos));
        } catch (IOException | ParseQueryException e) {
            logger.error(e.getMessage());
        }
        return pages;
    }

    public void indexPage(String url) {
        runMultithreadParse(url);
    }

    private void runMultithreadParse(String url) {
        ForkJoinPool fjp = new ForkJoinPool();
        RecursiveParseAction parseAction = new RecursiveParseAction(pageParserService,
                START_DEPTH,
                MAX_DEPTH,
                url,
                new HashSet<>());
        fjp.invoke(parseAction);
    }
}
