package org.andrews.abcsearch.search;

import org.andrews.abcsearch.pageparser.PageInfo;

import java.util.List;
import java.util.stream.Collectors;

public class PageMapper {

    private PageMapper() {
    }

    public static List<Page> mapToPage(List<PageInfo> pageInfos) {
        return pageInfos.stream()
                .map(Page::new)
                .collect(Collectors.toList());
    }

}
