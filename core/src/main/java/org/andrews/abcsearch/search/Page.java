package org.andrews.abcsearch.search;

import org.andrews.abcsearch.pageparser.PageInfo;

public class Page {

    private String title;
    private String description;
    private String query;

    public Page(String title, String description, String query) {
        this.title = title;
        this.description = description;
        this.query = query;
    }

    public Page(PageInfo info) {
        this.title = info.getTitle();
        this.description = info.getDescription();
        this.query = info.getQuery();
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getQuery() {
        return query;
    }

}
