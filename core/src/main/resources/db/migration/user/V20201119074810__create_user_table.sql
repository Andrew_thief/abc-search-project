--
-- Name: application_user; Type: TABLE;
--

CREATE TABLE application_user
(
    id           SERIAL PRIMARY KEY,
    email        VARCHAR(50) UNIQUE NOT NULL,
    first_name   VARCHAR(255)       NOT NULL,
    last_name    VARCHAR(255)       NOT NULL,
    password     VARCHAR(255)       NOT NULL,
    authority    VARCHAR(50)                 DEFAULT 'ROLE_USER'
);
