DROP TABLE history_record;

CREATE TABLE history_record
(
    id                   SERIAL PRIMARY KEY,
    application_user     INTEGER REFERENCES application_user (id),
    application_user_key INTEGER,
    url                  VARCHAR(255) NOT NULL,
    title                VARCHAR(255) NOT NULL,
    creation_date        TIMESTAMP
);
