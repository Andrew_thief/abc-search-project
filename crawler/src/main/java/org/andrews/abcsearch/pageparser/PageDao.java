package org.andrews.abcsearch.pageparser;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Component
public class PageDao {

    private final String DIR;

    public PageDao(@Value("${user-home}") String dir) {
        DIR = dir;
    }

    public List<PageInfo> search(String keywords, int page) throws ParseQueryException, IOException {
        List<Document> documents = new ArrayList<>();

        try (Directory dir = FSDirectory.open(Paths.get(DIR));
             IndexReader reader = DirectoryReader.open(dir)
        ) {
            IndexSearcher searcher = new IndexSearcher(reader);

            Query query = getQuery(keywords).orElseThrow(() -> new ParseQueryException("Unexpected parse error! Parse result is not presented."));
            TopDocs docs = searcher.search(query, page);
            ScoreDoc[] scoreDocs = docs.scoreDocs;

            for (ScoreDoc scoreDoc : scoreDocs) {
                int docId = scoreDoc.doc;
                Document d = searcher.doc(docId);
                documents.add(d);
            }
        } catch (ParseException e) {
            throw new ParseQueryException(e.getMessage());
        }

        return mapToPageInfos(documents);
    }

    public synchronized void save(String title, String body, String url) throws IOException {
        try (Directory dir = FSDirectory.open(Paths.get(DIR));
             Analyzer analyzer = new StandardAnalyzer();
             IndexWriter writer = new IndexWriter(dir, new IndexWriterConfig(analyzer));
        ) {
            Document document = createDocument(title, body, url);
            writer.addDocument(document);
        }
    }

    private Document createDocument(String title, String body, String url) {
        Document document = new Document();
        document.add(new TextField("title", title, Field.Store.YES));
        document.add(new StringField("body", body.substring(0, 150), Field.Store.YES));
        document.add(new StringField("url", url, Field.Store.YES));
        return document;
    }

    private Optional<Query> getQuery(String keywords) throws ParseException {
        Optional<Query> query;
        try (Analyzer analyzer = new StandardAnalyzer()) {
            QueryParser parser = new QueryParser(Version.LUCENE_8_6_3, "title", analyzer);
            query = Optional.of(parser.parse(keywords));
        }
        return query;
    }

    private List<PageInfo> mapToPageInfos(List<Document> documents){
            return documents.stream()
            .map(DocumentInfoMapper::mapToObj)
                    .collect(Collectors.toList());
    }
}
