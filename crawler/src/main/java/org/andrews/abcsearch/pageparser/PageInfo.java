package org.andrews.abcsearch.pageparser;

public class PageInfo {

    private String title;
    private String description;
    private String query;

    protected PageInfo(String title, String description, String query) {
        this.title = title;
        this.description = description;
        this.query = query;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getQuery() {
        return query;
    }
}
