package org.andrews.abcsearch.pageparser;

import org.jsoup.nodes.Document;

import java.util.*;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveAction;


public class RecursiveParseAction extends RecursiveAction {
    private final PageParserService pageParserService;
    private int currentDepth;
    private final int maxDepth;
    private final Collection<String> alreadyUsedUrls;
    private final String url;

    public RecursiveParseAction(PageParserService pageParserService,
                                int currentDepth, int maxDepth, String url,
                                Collection<String> alreadyUsedUrls) {
        this.pageParserService = pageParserService;
        this.currentDepth = currentDepth;
        this.maxDepth = maxDepth;
        this.alreadyUsedUrls = Collections.synchronizedSet(new HashSet<>(alreadyUsedUrls));
        this.url = url;
    }

    @Override
    protected void compute() {
        if (currentDepth < maxDepth && !alreadyUsedUrls.contains(url)) {
            alreadyUsedUrls.add(url);

            Document document = pageParserService.getPage(url);
            Collection<String> relatedUrls = pageParserService.analizePage(document, url);
            currentDepth += 1;

            ForkJoinTask.invokeAll(createSubTasks(relatedUrls));
        }
    }

    private List<RecursiveParseAction> createSubTasks(Collection<String> relatedUrls) {
        List<RecursiveParseAction> subTasks = new ArrayList<>();

        relatedUrls.removeAll(alreadyUsedUrls);

        alreadyUsedUrls.addAll(relatedUrls);
        for (String relatedUrl : relatedUrls) {
            subTasks.add(
                    new RecursiveParseAction(
                            pageParserService,
                            currentDepth,
                            maxDepth,
                            relatedUrl,
                            alreadyUsedUrls));
        }
        return subTasks;
    }

}
