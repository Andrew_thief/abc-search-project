package org.andrews.abcsearch.pageparser;

import org.apache.lucene.document.Document;

class DocumentInfoMapper {

    private DocumentInfoMapper() {
    }

    public static PageInfo mapToObj(Document doc) {
        String title = doc.getField("title").stringValue();
        String desc = doc.getField("description").stringValue();
        String query = doc.getField("query").stringValue();

        return new PageInfo(title, desc, query);
    }

}
