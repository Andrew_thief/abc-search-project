package org.andrews.abcsearch.pageparser;

public class ParseQueryException extends Exception {
    public ParseQueryException(String message) {
        super(message);
    }
}
