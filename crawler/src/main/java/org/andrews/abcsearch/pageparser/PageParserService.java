package org.andrews.abcsearch.pageparser;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class PageParserService {

    private final String userAgent;

    private final String referrer;

    private final PageDao pageDao;

    private final Logger logger;

    public PageParserService(@Value("${user-agent}") String userAgent,
                             @Value("${referrer}") String referrer,
                             PageDao pageDao,
                             Logger logger) {
        this.userAgent = userAgent;
        this.referrer = referrer;
        this.pageDao = pageDao;
        this.logger = logger;
    }

    public synchronized Set<String> analizePage(Document document, String url) {
        savePage(document, url);
        return getChildLinks(document);
    }

    public Document getPage(String url) {
        try {
            return Jsoup.connect(url)
                    .userAgent(userAgent)
                    .referrer(referrer)
                    .get();
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        return null;
    }

    private Set<String> getChildLinks(Document document) {
        Elements links = document.getElementsByTag("a");
        return links.stream()
                .map(element -> element.attr("abs:href"))
                .collect(Collectors.toSet());
    }

    private synchronized void savePage(Document doc, String url) {
        try {
            String title = doc.title();
            String body = doc.body().text();
            if (isNotEmpty(body) && isNotEmpty(title)) {
                pageDao.save(title, body, url);
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

    }

    private boolean isNotEmpty(String str) {
        return !(str == null || str.isEmpty());
    }
}

