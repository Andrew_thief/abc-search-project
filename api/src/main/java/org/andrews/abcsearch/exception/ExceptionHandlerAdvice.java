package org.andrews.abcsearch.exception;

import org.andrews.abcsearch.exceptions.EmailInUseException;
import org.andrews.abcsearch.exceptions.IllegalOperationException;
import org.andrews.abcsearch.exceptions.PasswordDoesNotMatchException;
import org.andrews.abcsearch.exceptions.UserNotFoundException;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;

@ControllerAdvice
public class ExceptionHandlerAdvice {

    private final Logger log;

    public ExceptionHandlerAdvice(Logger log) {
        this.log = log;
    }

    @ExceptionHandler(PasswordDoesNotMatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ModelAndView handle(PasswordDoesNotMatchException ex) {
        log.warn(ex.getMessage());

        ModelAndView mav = new ModelAndView("error");
        mav.addObject("errorMessage", ex.getMessage());
        return mav;
    }

    @ExceptionHandler(EmailInUseException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ModelAndView handle(EmailInUseException ex) {
        log.warn(ex.getMessage());

        ModelAndView mav = new ModelAndView("error");
        mav.addObject("errorMessage", ex.getMessage());
        return mav;
    }

    @ExceptionHandler(IllegalOperationException.class)
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public ModelAndView handle(IllegalOperationException ex) {
        log.error(ex.getMessage());

        ModelAndView mav = new ModelAndView("error");
        mav.addObject("errorMessage", ex.getMessage());
        return mav;
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(Exception.class)
    public ModelAndView handle(Exception ex) {
        log.warn(ex.getMessage());

        ModelAndView mav = new ModelAndView("error");

        if (ex.getMessage() == null) {
            mav.addObject("errorMessage", "Error!User not authenticated");
        } else {
            mav.addObject("errorMessage", ex.getMessage());
        }
        return mav;
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(UserNotFoundException.class)
    public ModelAndView handle(UserNotFoundException ex) {
        log.warn(ex.getMessage());

        ModelAndView mav = new ModelAndView("error");
        mav.addObject("errorMessage", ex.getMessage());
        return mav;
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(IOException.class)
    public ModelAndView handle(IOException ex) {
        log.error(ex.getMessage());

        ModelAndView mav = new ModelAndView("error");
        mav.addObject("errorMessage", ex.getMessage());
        return mav;
    }

}
