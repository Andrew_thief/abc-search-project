package org.andrews.abcsearch.search;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/index")
public class IndexController {

    private final SearchService searchService;

    public IndexController(SearchService searchService) {
        this.searchService = searchService;
    }

    @GetMapping
    public String getSearchPage() {
        return "index";
    }

    @PostMapping
    public String indexPage(String url) {
        searchService.indexPage(url);
        return "search";
    }

}
