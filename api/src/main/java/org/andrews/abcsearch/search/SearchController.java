package org.andrews.abcsearch.search;

import org.andrews.abcsearch.history.HistoryRecord;
import org.andrews.abcsearch.history.HistoryService;
import org.andrews.abcsearch.user.ApplicationUser;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

import static java.util.Objects.nonNull;

@RestController
@RequestMapping("/search")
public class SearchController {

    private final HistoryService historyService;

    private final SearchService searchService;

    public SearchController(HistoryService historyService, SearchService searchService) {
        this.historyService = historyService;
        this.searchService = searchService;
    }

    @GetMapping
    public String getSearchPage() {
        return "search";
    }

    @PostMapping
    public String search(@AuthenticationPrincipal ApplicationUser user,
                         @RequestParam String query,
                         @RequestParam int page,
                         Model model) {
        List<Page> pages = searchService.search(query, page);

        if (nonNull(user)) {
            saveToHistory(query, user);
        }
        model.addAttribute("searchResults", pages);

        return "search";
    }

    private void saveToHistory(String query, ApplicationUser user) {
        HistoryRecord record = createRecord(query);
        historyService.saveRecord(user, record);
    }

    private HistoryRecord createRecord(String query) {
        HistoryRecord record = new HistoryRecord();

        record.setQuery(query);
        record.setCreationDate(LocalDateTime.now());

        return record;
    }
}
