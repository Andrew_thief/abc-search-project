package org.andrews.abcsearch.config.security;

import org.andrews.abcsearch.exceptions.PasswordDoesNotMatchException;
import org.andrews.abcsearch.user.ApplicationUser;
import org.andrews.abcsearch.user.ApplicationUserService;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class AuthenticateUserProvider implements AuthenticationProvider {


    private final ApplicationUserService userService;
    private final PasswordEncoder passwordEncoder;

    public AuthenticateUserProvider(ApplicationUserService userService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String email = authentication.getName();
        String password = authentication.getCredentials().toString();

        ApplicationUser user = userService.getByEmail(email);

        if ( passwordEncoder.matches(password, user.getPassword())) {
            return user;
        } else {
            throw new PasswordDoesNotMatchException("Password is not correct");
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}

