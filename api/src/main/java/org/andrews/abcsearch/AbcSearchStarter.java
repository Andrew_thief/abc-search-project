package org.andrews.abcsearch;


import org.andrews.abcsearch.config.SearchConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AbcSearchStarter {
    public static void main(String[] args) {
        SpringApplication.run(SearchConfig.class, args);
    }
}
