package org.andrews.abcsearch.user.mapper;

import org.andrews.abcsearch.user.ApplicationUser;
import org.andrews.abcsearch.user.dto.ProfileDto;
import org.andrews.abcsearch.user.dto.UserCreationDto;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public ApplicationUser toEntity(UserCreationDto object) {
        ApplicationUser user = new ApplicationUser();

        user.setEmail(object.getEmail());
        user.setFirstName(object.getFirstName());
        user.setLastName(object.getLastName());
        user.setPassword(object.getPassword());

        return user;
    }

    public ProfileDto toProfile(ApplicationUser user) {
        ProfileDto object = new ProfileDto();

        object.setEmail(user.getEmail());
        object.setFirstName(user.getFirstName());
        object.setLastName(user.getLastName());

        return object;
    }
}
