package org.andrews.abcsearch.user;

import org.andrews.abcsearch.exceptions.PasswordDoesNotMatchException;
import org.andrews.abcsearch.user.dto.CredentialsDto;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/login")
public class LoginController {

    private final ApplicationUserService userService;
    private final PasswordEncoder passwordEncoder;

    public LoginController(ApplicationUserService userService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping
    public String getLoginPage() {
        return "login";
    }

    @PostMapping
    public String login(CredentialsDto credentials, Model model) {

        try {
            ApplicationUser user = userService.getByEmail(credentials.getEmail());

            if (passwordEncoder.matches(credentials.getPassword(), user.getPassword())) {
                Authentication authenticationToken = new UsernamePasswordAuthenticationToken(credentials.getEmail(), credentials.getPassword());
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            } else {
                throw new PasswordDoesNotMatchException("Password is not correct");
            }

            return "redirect:/search";
        } catch (Exception e) {
            e.printStackTrace();
            model.addAttribute("loginError", true);
            model.addAttribute("errorMessage", e.getMessage());
            return "login";
        }
    }

}
