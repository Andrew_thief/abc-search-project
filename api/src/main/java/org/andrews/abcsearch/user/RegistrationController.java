package org.andrews.abcsearch.user;

import org.andrews.abcsearch.exceptions.EmailInUseException;
import org.andrews.abcsearch.exceptions.PasswordDoesNotMatchException;
import org.andrews.abcsearch.user.dto.RegistrationInfoDto;
import org.andrews.abcsearch.user.dto.UserCreationDto;
import org.andrews.abcsearch.user.mapper.UserMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/registration")
public class RegistrationController {

    private final ApplicationUserService userService;
    private final UserMapper mapper;
    private final BCryptPasswordEncoder passwordEncoder;

    public RegistrationController(ApplicationUserService userService,
                                  UserMapper mapper,
                                  BCryptPasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.mapper = mapper;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping
    public void registerNewUser(RegistrationInfoDto registrationInfoDto, HttpServletRequest request) throws ServletException {
        String email = registrationInfoDto.getEmail();
        checkEmailUsage(email);

        if (isPasswordsMatch(registrationInfoDto)) {
            UserCreationDto creationDto = createUserInfoObject(registrationInfoDto);

            ApplicationUser user = mapper.toEntity(creationDto);
            user.setAuthority(Authority.ROLE_USER);
            userService.save(user);

            request.login(email, registrationInfoDto.getPass1());
        } else {
            throw new PasswordDoesNotMatchException("Passwords does`t much. Please try again.");
        }
    }

    private void checkEmailUsage(String email) {
        if (userService.isEmailInUse(email)) {
            throw new EmailInUseException("User with this email already defined in system. Please enter another email!");
        }
    }

    private boolean isPasswordsMatch(RegistrationInfoDto registrationInfoDto) {
        String firstPhrase = registrationInfoDto.getPass1();
        String secondPhrase = registrationInfoDto.getPass2();

        return firstPhrase.equals(secondPhrase);
    }

    private UserCreationDto createUserInfoObject(RegistrationInfoDto dto) {
        UserCreationDto userInfo = new UserCreationDto();

        userInfo.setFirstName(dto.getFirstName());
        userInfo.setLastName(dto.getLastName());
        userInfo.setPassword(passwordEncoder.encode(dto.getPass1()));
        userInfo.setEmail(dto.getEmail());

        return userInfo;
    }
}
