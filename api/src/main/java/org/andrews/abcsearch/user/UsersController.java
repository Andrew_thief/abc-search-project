package org.andrews.abcsearch.user;

import org.andrews.abcsearch.user.dto.ProfileDto;
import org.andrews.abcsearch.user.mapper.UserMapper;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UsersController {

    private final UserMapper mapper;

    public UsersController( UserMapper mapper) {
        this.mapper = mapper;
    }

    @GetMapping("/me")
    public ProfileDto getUsersProfile(@AuthenticationPrincipal ApplicationUser user) {
        return mapper.toProfile(user);
    }

}